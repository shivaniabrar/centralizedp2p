package service

import (
	"log"
	"net"
	"net/http"
)

func GetListner() *net.TCPListener {

	addr, err := net.ResolveTCPAddr("tcp", ":0")
	if err != nil {
		log.Fatal(err)
	}

	listner, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	return listner
}

func StartFileServer(listner *net.TCPListener, dir string) {
	log.Fatal(http.Serve(listner, http.FileServer(http.Dir(dir))))
}
