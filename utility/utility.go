package utility

import (
	"bytes"
	"io/ioutil"
	"net"
	"os"
)

type Id struct {
	IP   string `json:"ip"`
	PORT string `json:"port"`
}

func GetIP() string {
	var ip string

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ip = ipnet.IP.String()
				break
			}
		}
	}
	return ip
}

func GetPort(listner *net.TCPListener) string {
	_, port, _ := net.SplitHostPort(listner.Addr().String())
	return port
}

func GetFilelist(path string) string {
	var buffer bytes.Buffer
	dir, _ := ioutil.ReadDir(path)
	for _, value := range dir {
		if value.IsDir() == false {
			buffer.WriteString(value.Name() + " ")
		}
	}
	return buffer.String()
}
