package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"strings"
	"utility"
	"utility/service"
	"path/filepath"
)

var metadata serverpacket
var menu = []string{"Replicate", "Obtain"}


type ids struct {
	IP   string `json:"ip"`
	PORT string `json:"port"`
}

type id struct {
	IP   string `json:"ip"`
	PORT string `json:"port"`
}

type rpcids struct {
	IP   string `json:"ip"`
	PORT string `json:"port"`
	RPC  string `json:"rpc_port"`
}

type serverpacket struct {
	ID                       id               `json:"id"`
	RPC_PORT                 string           `json:"rpcport"`
	FILELIST                 string           `json:"filelist"`
	FileServerListner        *net.TCPListener `json:"fileServerListner"`
	ReplicationServerListner *net.TCPListener `json:"ReplicationServerListner"`
	Shared_directory_path    string           `json:"path"`
	ServerID                 id
}

func (server_packet *serverpacket) Register() string {
	mymetadata := *server_packet
	server_msg := mymetadata.ID.IP + " " + mymetadata.ID.PORT +
		" " + mymetadata.RPC_PORT + " " + mymetadata.FILELIST
	return server_msg
}

func (server_packet *serverpacket) FileNameString(filename string) string {
	mymetadata := *server_packet
	server_msg := mymetadata.ID.IP + " " + mymetadata.ID.PORT +
		" " + mymetadata.RPC_PORT + " " + filename
	return server_msg
}

func (server_packet *serverpacket) ReplicasFileNameString(replicas string, filename string) string {
	mymetadata := *server_packet
	server_msg := replicas + "," + mymetadata.ID.IP + " " + mymetadata.ID.PORT +
		" " + filename
	return server_msg
}

func main() {

	/*
		Gets Paramaters from User
		Intializes metadata information
	*/
	initialize()

	/* Get Connection to Server */
	server_id := metadata.ServerID
	server, err := connect_server(server_id)

	if err != nil {
		fmt.Println("Cannot connect to: ",server_id.IP,server_id.PORT)
		os.Exit(1)
	}

	/* Start File Server for other peers to download */
	go startfileServer()

	/* Start Server for accepting connections for replication */
	go ReplicationServer(server)

	/* Register Server
	Sends ip,port and filelist to the server
	*/
	err = register_to_server(server)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Registered with the server")
	
	for {

		var filename string
		var choice int

		fmt.Print("\nEnter filename with extension: ")
		fmt.Scanf("%s", &filename)

		fmt.Println("\nWhat you would like to do?\n")
		for index, operation := range menu {
			/* Since index starts from 0 */
			fmt.Printf("%d. %s\n", index+1, operation)
		}

		fmt.Print("Enter your choice: ")
		fmt.Scanf("%d", &choice)

		switch {
		case choice == 1:
			replicate(server, filename)
		case choice == 2:
			obtain(server, filename)
		case choice > 2 || choice <= 0:
			fmt.Println("Invalid Input")
		}
	}
	
}

/*
	Get Parameters from User
*/
func getUserParamaters() (id, string) {
	var server_id id

	ip := flag.String("ip", "", "ip of server")
	port := flag.String("port", "", "port of server")
	shared_directory_path := flag.String("path", ".", "path of shared directory")

	flag.Parse()

	server_id.IP = *ip
	server_id.PORT = *port

	if (*ip=="" || *port==""){
		fmt.Println("Usage: client -ip=<server's ip> -port=<server's port> -path=<path of shared directory>")
		os.Exit(1)
	}

	return server_id, *shared_directory_path
}

/*
	Initialize Metadata Information of Peer
*/

func initialize() {
	var myId id

	server_id, shared_directory_path := getUserParamaters()
	FileServerListner := service.GetListner()
	ReplicationServerListner := service.GetListner()

	myId.IP = utility.GetIP()
	myId.PORT = utility.GetPort(FileServerListner)

	metadata.ID = myId
	metadata.FileServerListner = FileServerListner
	metadata.ReplicationServerListner = ReplicationServerListner
	metadata.RPC_PORT = utility.GetPort(ReplicationServerListner)
	metadata.FILELIST = utility.GetFilelist(shared_directory_path)
	metadata.ServerID = server_id
	metadata.Shared_directory_path = filepath.Clean(shared_directory_path)
}

/*
	Replicate on other peers
*/
func replicate(server *rpc.Client, filename string) {
	var replicas string
	var reply bool

	fmt.Println("Enter number of replicas: ")
	fmt.Scanf("%s", &replicas)

	error := server.Call("Listener.Replicate", metadata.ReplicasFileNameString(replicas, filename), &reply)

	if error != nil {
		fmt.Println(error)
	}

	if reply {
		fmt.Println("Replicated Successfully: ",filename)
	}else{
		fmt.Println("Cannot replicate",filename)
	}

}

/*
	Search file on Index Server
	Downloads file from other peers
*/
func obtain(server *rpc.Client, filename string) {
	var peer_index int
	peers := searchfile(server, filename)

	if peers == nil {
		fmt.Println("No peer is having the file")
		return
	}

	fmt.Printf("Peers:\n")
	for index, peer := range peers {
		fmt.Printf("%d. %s %s\n", index+1, peer.IP, peer.PORT)
	}

	fmt.Print("From which peer you want to download file?")
	no_of_peers := len(peers)
	fmt.Scanf("%d", &peer_index)
	if peer_index <= 0 || peer_index > no_of_peers {
		fmt.Println("Invalid Option")
		return
	}

	peer := ids{peers[peer_index-1].IP, peers[peer_index-1].PORT}

	downloadfile(server, peer, filename)
}

/*
	Downloads file from selected peer
	Updates Index Server if Download is Successful
*/
func downloadfile(server *rpc.Client, peer ids, filename string) {
	var reply bool
	response, err := http.Get("http://" + peer.IP + ":" + peer.PORT + "/" + filename)
	if err != nil {
		fmt.Println("Unable to download file from peer : ",peer.IP,peer.PORT)
		return
	} else {
		defer response.Body.Close()
		out, err := os.Create(metadata.Shared_directory_path+"/"+filename)
		if err != nil {
			return
		}
		defer out.Close()
		io.Copy(out, response.Body)
		fmt.Printf("Download Successful: %s\n", filename)

		line := metadata.FileNameString(filename)
		err = server.Call("Listener.Updatefilelist", line, &reply)
		if err != nil {
			log.Fatal(err)
		}

	}
}

/*
	Search file and return peers list containig the file
*/
func searchfile(server *rpc.Client, filename string) []rpcids {

	var reply []byte
	var id []rpcids
	err := server.Call("Listener.Searchfile", filename, &reply)
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(reply, &id)
	return id
}

/*
	Establishes Connection to the Server
*/
func connect_server(server_id id) (*rpc.Client, error) {
	return rpc.Dial("tcp", server_id.IP+": "+server_id.PORT)
}

/*
	Sends IP, Port and Filelist to the server
*/
func register_to_server(server *rpc.Client) error {
	var reply bool
	return server.Call("Listener.Register", metadata.Register(), &reply)
}

/* Servers */

/*
	Server for handling request for replicating the file
*/
func ReplicationServer(server *rpc.Client) {
	listener := new(Listener)
	listener.server = server
	rpc.Register(listener)
	rpc.Accept(metadata.ReplicationServerListner)
}

/*
	Server for handling request for obtaining the file
*/
func startfileServer() {
	service.StartFileServer(metadata.FileServerListner, metadata.Shared_directory_path)
}

/*
	Handles Replication for the file
*/

type Listener struct {
	server *rpc.Client
}

func (l *Listener) Replicate(line string, ack *bool) error {
	request := strings.Split(string(line), " ")
	ip := request[0]
	port := request[1]
	filename := request[2]
	identity := ids{ip, port}
	downloadfile(l.server, identity, filename)
	return nil
}
