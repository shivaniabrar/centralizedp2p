package main

import (
	"container/list"
	"encoding/json"
	"fmt"
	"net/rpc"
	"strconv"
	"strings"
	"utility"
	"utility/service"
)

type ids struct {
	IP   string `json:"ip"`
	PORT string `json:"port"`
	RPC  string `json:"rpc_port"`
}

var index map[string][]ids
var clients *list.List

type Listener int

var ip string

/*
	Registers Client
	Index: Associates filenames with clients
*/
func (l *Listener) Register(line string, ack *bool) error {
	request := strings.Split(string(line), " ")
	ip := request[0]
	port := request[1]
	rpc_port := request[2]
	id := ids{ip, port, rpc_port}
	clients.PushFront(id)
	for i := 2; i < len(request); i++ {
		index[request[i]] = append(index[request[i]], id)
	}
	fmt.Printf("\n Peer<%s,%s> Registered", ip, port)
	return nil
}

/*
	Search files in Index
*/
func (l *Listener) Searchfile(line string, ack *[]byte) error {
	peers, _ := index[line]
	b, _ := json.Marshal(peers)
	*ack = b
	return nil
}

/*
	Updates Index
	If file download or replicated index must be updated
*/
func (l *Listener) Updatefilelist(line string, ack *bool) error {
	request := strings.Split(string(line), " ")
	ip := request[0]
	port := request[1]
	rpc_port := request[2]
	id := ids{ip, port, rpc_port}
	index[request[3]] = append(index[request[3]], id)
	return nil
}

/*
	Informs Other Peers to Replicate file
*/
func (l *Listener) Replicate(line string, ack *bool) error {
	var reply bool
	*ack = true
	request := strings.Split(string(line), ",")

	/*
		Get Number of Replicas
	*/
	replicas, err := strconv.Atoi(request[0])
	
	if err!=nil{
		*ack = false
		return nil
	}

	if replicas<=0 {
		*ack = false
		return nil	
	}
	
	clientInfo := strings.Split(string(request[1]), " ")

	/*
		Get filename from request line
	*/
	filename := clientInfo[2]

	authenticate := true
	for _, clientIds := range index[filename] {
		if ((clientIds.IP == clientInfo[0]) && (clientIds.PORT == clientInfo[1]))  == true {
				authenticate = false
				break
		}
	}

	if authenticate {
		*ack = false
		return nil
	}

	/*
		Get total number of clients currently having the file
	*/
	no_of_clients := len(index[filename])
	/*
		Get total number of clients registered
	*/
	total_clients := clients.Len()

	if no_of_clients == replicas {
		return nil
	}

	if total_clients < replicas {
		*ack=false
		return nil
	}

	required_clients := total_clients - no_of_clients

	for _, clientIds := range index[filename] {
		for e := clients.Front(); e != nil; e = e.Next() {
			if required_clients <= 0 {
					break
				}
			if ((clientIds.IP == (e.Value.(ids)).IP) && (clientIds.PORT == (e.Value.(ids)).PORT)) == false {
				client, err := rpc.Dial("tcp", (e.Value.(ids)).IP+": "+(e.Value.(ids)).RPC)
				if err != nil {
					*ack = false
					return nil
				}
				err = client.Call("Listener.Replicate", request[1], &reply)
				if err != nil {
					*ack = false
					return nil
				}
				required_clients -= 1	
			}
		}
	}

	return nil
}

func main() {
	index = make(map[string][]ids)
	clients = list.New()
	tcpListner := service.GetListner()
	listener := new(Listener)

	fmt.Println("Server Information")
	fmt.Printf("IP: %s Port:%s\n\n", utility.GetIP(), utility.GetPort(tcpListner))

	rpc.Register(listener)
	rpc.Accept(tcpListner)

}
